defmodule Helloer.Router do
  use Helloer.Web, :router

  get "/hello", Helloer.HelloController, :show
end
