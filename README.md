# Helloer

Use [Erlang Solutions](https://packages.erlang-solutions.com/erlang/) packages to install latest Erlang and Elixir releases.

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Compile everything for production: `MIX_ENV=prod mix do compile, phoenix.digest`
  * Start Phoenix endpoint with `PORT=4000 MIX_ENV=prod elixir --erl "+K true" -S mix phoenix.server`

Files will be server from `priv/static` subfolder.
