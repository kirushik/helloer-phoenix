defmodule Helloer.Endpoint do
  use Phoenix.Endpoint, otp_app: :helloer

  plug Plug.Static, at: "/", :helloer
end
